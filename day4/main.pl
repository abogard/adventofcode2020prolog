:- use_module(library(pcre)).
:- use_module("../utils/helpers.pl", [lines_to_list/2]).

read_file(O):- 
    read_file_to_string('day4/part1.txt', S, []),
    re_split("\n\n", S, M),
    exclude(==("\n\n"), M, O).

fields(X):- list_to_set(["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"], X).

rule("ecl", "amb").
rule("ecl", "blu").
rule("ecl", "brn").
rule("ecl", "gry").
rule("ecl", "grn").
rule("ecl", "hzl").
rule("ecl", "oth"). 
rule("byr", S):- re_matchsub("^(?<num_T>\\d{4})$", S, N, []), between(1920, 2002, N.num).
rule("iyr", S):- re_matchsub("^(?<num_T>\\d{4})$", S, N, []), between(2010, 2020, N.num).
rule("eyr", S):- re_matchsub("^(?<num_T>\\d{4})$", S, N, []), between(2020, 2030, N.num).
rule("hgt", S):- re_matchsub("^(?<num_T>\\d+)cm$", S, N, []), between(150, 193, N.num).
rule("hgt", S):- re_matchsub("^(?<num_T>\\d+)in$", S, N, []), between(59, 76, N.num).
rule("hcl", S):- re_match("^#([a-f0-9]){6}$", S).
rule("pid", S):- re_match("^\\d{9}$", S).

has_fields(S):- split_string(S, ":\n ","", T), fields(F), intersection(F, T, F).
is_field_valid(S, K):- split_string(S, ":\n ","", T), append(_, [K, V| _], T), rule(K, V).
are_fields_valid(S):- setof(K, is_field_valid(S, K), F), fields(F).

part1:- read_file(I), include(has_fields, I, O), length(O, C), format("Part 1: ~a\n", C).
part2:- read_file(I), include(are_fields_valid, I, O), length(O, C), format("Part 2: ~a\n", C).

run:- part1, part2.