:- use_module("../utils/helpers.pl", [lines_to_list/2, times/3]).
:- use_module(library(list_util)).

read_map(M):- lines_to_list('day3/part1.txt', L), maplist(string_chars, L, M).

find_tree(X, Y, N):- read_map(M), nth0(X, M, I), length(I, N), nth0(Y, I, '#').
find_rule(R, C):- find_tree(X, Y, N), divmod(X, R, XS, 0), Y =:= XS * C mod N.
find_count(R, C, Count):- aggregate_all(count, find_rule(R, C), Count).

rule(C):- find_count(1, 1, C).
rule(C):- find_count(1, 3, C).
rule(C):- find_count(1, 5, C).
rule(C):- find_count(1, 7, C).
rule(C):- find_count(2, 1, C).



part1:- find_count(1, 3, C), format("Part 1: ~a", C).
part2:- bagof(C, rule(C), R), foldl(times, R, 1, N), format("Part 2: ~a", N).