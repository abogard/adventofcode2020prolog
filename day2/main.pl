:- use_module(library(pcre)).
:- use_module(library(apply)).
:- use_module("../utils/helpers.pl", [lines_to_list/2, idx/3, xor/2]).

rule(min, max, letter, password).

to_rule(X, rule(MIN, MAX, L, P)):- 
    number_string(MIN, X.min),
    number_string(MAX, X.max),
    string_chars(X.letter, [L]),
    string_chars(X.password, P).

pattern(I, O):- re_matchsub("(?<min>\\d+)[-](?<max>\\d+) (?<letter>.): (?<password>.+)", I, O, []).

read_rules(R):- lines_to_list('day2/part1.txt', L),
    maplist(pattern, L , M),
    maplist(to_rule, M, R).

validate_reps(rule(MIN, MAX, LETTER, PASSWORD)):- 
    aggregate_all(count, nth1(_, PASSWORD, LETTER), C),
    between(MIN, MAX, C).

validate_indices(rule(MIN, MAX, LETTER, PASSWORD)):-
    bagof(I, nth1(I, PASSWORD, LETTER), S),
    xor(member(MIN, S), member(MAX, S)). 

part1:- read_rules(R), include(validate_reps, R, V), length(V, L), format("Part 1: ~a\n", L).
part2:- read_rules(R), include(validate_indices, R, V), length(V, L), format("Part 2: ~a\n", L).

run:- part1, part2.