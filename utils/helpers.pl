:-module(helpers, [lines_to_list/2, to_numbers/2, idx/3, xor/2, times/3]).
:-use_module(library(readutil)).

lines_to_list(Filename, L):- read_file_to_string(Filename, S,[]), split_string(S, "\n", "", L).

to_numbers(StringsList, NumsList):-maplist(number_string, NumsList, StringsList).

idx(L, N, O):- nth1(N, L, O, _).

xor(X, Y):- X, not(Y).
xor(X, Y):- Y, not(X).

times(X, Y, Z):- Z is X * Y.