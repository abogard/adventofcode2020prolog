:- use_module("../utils/helpers.pl", [lines_to_list/2, to_numbers/2]).

read_numbers(Nums):- lines_to_list('day1/part1.txt', S), to_numbers(S, Nums).

twosum(L, H1, H2, S):- nth0(I1, L, H1), nth0(I2, L, H2), I1 =\= I2, H1 + H2 =:= S.
threesum(L, H1, H2, H3):- nth0(_, L, H1, O), twosum(O, H2, H3, 2020 - H1).

part1:-read_numbers(N), twosum(N, X, Y, 2020), R is X * Y, format("Part 1: ~a\n", R).
part2:-read_numbers(N), threesum(N, X, Y, Z), R is X * Y * Z, format("Part 2: ~a\n", R).

run:- part1, part2.
